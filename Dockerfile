FROM python:3.9-alpine as pythonBuilder

WORKDIR /planet-express-heimdall

ADD requirements.txt .

RUN \
 apk add --no-cache postgresql-libs postgresql-dev postgresql-client && \
 apk add --no-cache --virtual .build-deps gcc musl-dev  && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

COPY ./src/peheimdall /planet-express-heimdall

ENTRYPOINT [ "gunicorn", "peheimdall.wsgi", "-b", "0.0.0.0:8000" ] 
            