from requests import get

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework_simplejwt.authentication import JWTAuthentication

from peheimdall.settings import PLANET_EXPRESS_ABE_URI, PLANET_EXPRESS_ABE_TIMEOUT


def request_abe(endpoint):
    try:
        response_graph = get(endpoint, timeout=int(PLANET_EXPRESS_ABE_TIMEOUT))
        json_graph = response_graph.json()
        status_code = response_graph.status_code
    except Exception as e:
        json_graph = {'message': "Error connection with our internal API"}
        status_code = 500

    return json_graph, status_code

class ScoreView(APIView):
    """
    * Requires token authentication.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, topics, degrees, score):
        """
        """
        json_graph, status_code = request_abe("{}/api/topics/{}/degrees/{}/score/{}".format(
            PLANET_EXPRESS_ABE_URI,
            topics,
            degrees,
            score
        ))
        return Response(json_graph, status=status_code)

class ScoreMinMaxView(APIView):
    """
    * Requires token authentication.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, topics, degrees, score_min, score_max):
        """
        """
        json_graph, status_code = request_abe("{}/api/topics/{}/degrees/{}/score_min/{}/score_max/{}".format(
            PLANET_EXPRESS_ABE_URI,
            topics,
            degrees,
            score_min,
            score_max
        ))
        return Response(json_graph, status=status_code)


class ScoreMinMaxDatesView(APIView):
    """
    * Requires token authentication.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, topics, degrees, score_min, score_max, date_min, date_max):
        """
        """
        json_graph, status_code = request_abe("{}/api/topics/{}/degrees/{}/score_min/{}/score_max/{}/date_min/{}/date_max/{}".format(
            PLANET_EXPRESS_ABE_URI,
            topics,
            degrees,
            score_min,
            score_max,
            date_min,
            date_max
        ))
        return Response(json_graph, status=status_code)

class UrlsView(APIView):
    """
    * Requires token authentication.
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, urls_ids):
        """
        """
        json_graph, status_code = request_abe("{}/api/urls/{}".format(
            PLANET_EXPRESS_ABE_URI,
            urls_ids
        ))
        return Response(json_graph, status=status_code)