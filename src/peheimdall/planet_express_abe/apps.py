from django.apps import AppConfig


class PlanetExpressAbeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'planet_express_abe'
