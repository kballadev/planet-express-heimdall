from django.urls import path

from . import views

urlpatterns = [
    path('topics/<topics>/degrees/<degrees>/score/<score>',
        views.ScoreView.as_view(), name='score'),
    path('topics/<topics>/degrees/<degrees>/score_min/<score_min>/score_max/<score_max>',
        views.ScoreMinMaxView.as_view(), name='score_min_max'),
    path('topics/<topics>/degrees/<degrees>/score_min/<score_min>/score_max/<score_max>/date_min/<date_min>/date_max/<date_max>',
        views.ScoreMinMaxDatesView.as_view(), name='score_min_max_date'),
    path('urls/<urls_ids>',
        views.UrlsView.as_view(), name='urls')        
]