from requests import get

from rest_framework.views import APIView
from rest_framework.response import Response


class HealthView(APIView):
    """
    * Requires token authentication.
    """

    authentication_classes = []
    permission_classes = []

    def get(self, request):
        """
        """
        
        return Response({
            "status": "OK"
        })