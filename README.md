#PYTHON 
pip install django psycopg2 djangorestframework markdown django-filter djangorestframework-simplejwt


#Database postgres
CREATE DATABASE planet_express_heimdall
ALTER ROLE postgres SET client_encoding TO 'utf8';
ALTER ROLE postgres SET default_transaction_isolation TO 'read committed';
ALTER ROLE postgres SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE planet_express_heimdall TO postgres;

#Configure migrations
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser